what remains to be done:
* Customizing CSS to give the thing the proper dunish look.
* Adding CSS classes for news item and module short summary.
* Resolve the clashes between the main and doxygen CSS
* Dig through content for broken links
* Add aliases to the front matter where backwards compatibility is desired
* Write meaningful texts about in content/groups
* Write module pages for all existing Dune modules
* Discard outdated content, I have not taken such measures yet.
* Decide what to do with the gallery: Should it be integrated with the modules
  archetype or be separate?
* Add a 256x256 logo named aaple-touch-icon.png to /static
* Generate and include Sphinx-based build-system documentation
