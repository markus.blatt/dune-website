+++
title = "Buildsystem Docs"
layout = "buildsystem"
[menu.main]
parent = "docs"
identifier = "buildsystem"
weight = 5
+++

# DUNE Buildsystem Documentation

Beginning from version 2.4, DUNE's CMake build system is documented with Sphinx.
Please choose the documentation you want to view from the following list.
