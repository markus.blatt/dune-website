+++
date = "2008-04-09T16:03:40+01:00"
major_version = 1
minor_version = 1
modules = ["dune-common", "dune-istl", "dune-grid", "dune-grid-howto"]
patch_version = 0
title = "Dune 1.1.0"
version = "1.1.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.1 - Release notes

## Changes to previous release DUNE 1.0

The changes to the previous release DUNE 1.0 are the following:

*   **dune-common**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1/diff-dune-common-1.1.patch) generated with diff.

    *   Several bug fixes (such as method mv of FieldMatrix)
    *   Change of the build system (Dependencies of modules are extracted from dune.module)
    *   Tuple is removed, the new implementation mimicks the std::tr1::tuple
    *   Dune::Array, Dune::DList, Dune::FixedArray, Dune::RemoveConst, and Dune::SameType are deprecated

*   **dune-grid**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1/diff-dune-grid-1.1.patch) generated with diff.

    *   Several bug fixes
    *   UGGrid is usable with non-conform refinement
    *   The constructor of UGGrid with two integer arguments is deprecated
    *   DGFParser can read periodic (YaspGrid) and closure (UGGrid)

*   **dune-istl**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1/diff-dune-istl-1.1.patch) generated with diff.

## Tested Platforms

The DUNE core developers have tested DUNE on the following platforms.

*   Debian Linux Sarge and Etch, using mpich/lam and gcc (3.4, 4.0, 4.1)
*   Gentoo Linux, using openmpi/mpich and gcc (4.1, 4.2)
*   HP XC Version 3.0/SFS 2.1 using gcc (3.4 ,4.0)
*   SuSE Linux 10.x using gcc

We have also gotten reports of successful installations on various flavours of Mac OSX.

## Known Issues

Here is an (incomplete) list of things that we know not to work in DUNE 1.1.

### Issues Applying to all modules

*   config.h is needed as first include in all applications
*   Out of source builds are not possible right now.
*   The build system only recognizes dependendies on a fixed set of known modules. Besides the core Dune modules these are `dune-disc`, `dune-fem`, and `dune-subgrid`.
*   The build system does not work with Solaris `make`. Please use GNU `make` instead.
*   DUNE doesn't install on Windows platforms, not even using CygWin.
*   So far DUNE has not been developed with thread-safety in mind, and you're probably bound for trouble if you try multi-threaded code.

### Issues of dune-common

None that we know of.

### Issues of dune-grid

*   #### `AlbertaGrid`:

    *   Only ALBERTA version 1.2 is supported
    *   LevelIntersectionIterator not working correctly and therefore LevelIterators and LevelIntersectionIterator disabled.
    *   `AlbertaGrid` only implemented for dimension = dimensionworld
    *   `AlbertaGrid` only implemented for dimension = 2,3
    *   `AlbertaGrid` cannot be used with different dimensions in one program, i.e. a 2d grid and a 3d grid.
    *   `AlbertaGrid` not usable for parallel computations
*   #### `ALUCubeGrid`:

    *   only implemented for dimension 3.
*   #### `ALUSimplexGrid`:

    *   implemented for dimension 2 and 3
    *   only 3d version usable for parallel computations
*   #### `ALUConformGrid`:

    *   only implemented for dimension 2
    *   not usable for parallel computations
*   #### `SGrid`:

    *   for debugging only since `SGrid` is not working as fast as YaspGrid.
    *   not usable in parallel computations
    *   insufficient support of boundary ids
*   #### `YaspGrid`:

    *   YaspGrid does not work in 1d.
    *   SubEntities only supported for codimension = dimension, i.e. Vertices
    *   The left lower corners always is the origin 0
    *   insufficient support of boundary ids
*   #### `UGGrid`:

    *   not usable for parallel computations yet
    *   SubEntities only supported for codimension = dimension, i.e. Vertices
    *   Ids not supported for all sub entity types
    *   insufficient support of boundary ids
*   #### `OneDGrid`:

    *   not usable for parallel computations
    *   adaptation cycle interface not fully supported (see [flyspray task #324](http://hal.iwr.uni-heidelberg.de/flyspray/?do=details&id=324))
*   #### `DGFParser`

    *   Only conform initial grids are supported
    *   No mixed element types can be used
    *   No support for distributed initial grid structures
*   #### `VTKWriter`:

    *   binary appended mode will not work on 64 bit machine

### Issues of dune-grid-howto

*   Description of Leaf/LevelIterator is missing.
*   UnitCube still uses deprecated heap size for instantiating UGGrid
*   No explanation what happens if Sgrid gets instantiated with dim<dimworld (see [Flyspray issue 208](http://hal.iwr.uni-heidelberg.de/flyspray/?do=details&id=208))
*   grids/unitcube1.gdf is not a real 1d grid

### dune-istl

*   The rowdim Function does not count empty rows ([FlySpray #7](http://hal.iwr.uni-heidelberg.de/flyspray/?do=details&id=7))
*   Sometimes old c-headers are still used (-> Not fully standard compliant)
*   Output still uses C-style printf
*   Smoothers of `AMG` do not support block level.

For known bugs, please take a look at the [bug tracking system](../flyspray.html) and the documentation!
