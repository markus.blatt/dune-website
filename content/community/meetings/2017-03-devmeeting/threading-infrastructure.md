+++
title = "Dune Developer Meeting 2017 -- Threading Infrastructure"
+++

Participants: Jö, Steffen, Christian, Markus, Ansgar, Robert

Programming Modell
------------------

Which Programming model shall we use?  Contestants are threads and tasks.

In case of tasks:
- They should be optional
- We need that the thread pool for tasks can be limited, so the user can (temporarily) set a number of threads aside for external libraries which use threads.
- We need something akin to `std::async()` (TBB provides something)
- Everyone in the WG agrees

Plenary vote: Tasks: 8, other: 0, abstain: 2

API
---

How much do we bind ourselves to a particular tasking API?

Contestants are TBB and OpenMP, but nobody likes OpenMP, so really only TBB.

- Licence: TBB recently switched to Apache 2, is that compatible with our
  license?  It is if we finally switch to LGPL3, in the current state
  compatibility is unclear.
- We could introduce an abstraction layer.  But that only makes sense if we
  have at least a rough idea regarding the possible variability of the
  interface.  We do not know about any other possible interface.  And wrapping
  will in some cases be quite expensive (support wise).
- Even if we standardize on TBB, OpenMP should still be usable since both use
  the same thread pool.

Proposal of the WG:
- We standardise on TBB without abstraction/wrappers.  TBB remains optional: if
  not available there will be not automatic threading in the core modules.
  Infrastructure that only makes sense in connection with TBB may be missing
  then.
- We do not allow OpenMP in core modules.  Users may use OpenMP in their own
  modules.
- We generally disallow threads not managed by the thread pool.  In special
  cases you may start threads outside (e.g. for communicating in the
  background), but you need to ensure that there are no adverse effects, e.g.
  performace loss due to oversubscription.
- When there is equivalent functionality in TBB and the C++ standard, prefer
  the standard functionality.

Plenary Vote:
- Standardise in TBB in the core as proposed: Yes: 8, No: 0, abstain 3
- Prefer equivalent functionality from `std::`: Yes: 9, No: 0, abstain: 4

Integration Plan
----------------
- EXADUNE will make a merge request for the baseic TBB infrastructure (e.g.
  build system)
- EXADUNE will then sucessively migrate TBB-dependent features into the core

Open Questions
--------------
- Is there a minimum TBB version we should require  
  (Steffen: 4.3 provides task observer, which are required for pinning.)  
  Well probably introduce restrictions as they turn out to be required.
- Initializing of TBB?  By default it uses all threads, which is bad if you
  start your program with `mpirun -n $ncpu ...`.

