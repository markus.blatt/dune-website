Dune User Meeting 2015 in Heidelberg
====================================

Invitation
----------

We invite all Dune users to participate at the 3rd Dune User meeting, to
be held at Heidelberg from 28.09.2015 morning to 29.09.2015 noon.

This is your, the users’ meeting and should be used to showcase how you
are using DUNE, foster future collaborations between the users, and the
users and developers, and provide input for the future development of
DUNE. Therefore we keep the format rather informal. All participants
should indicate if they want to give a presentation. In addition, we
will reserve plenty of time for more general discussions on Dune.

Date
----

September 28-29, 2015\
starts on Monday morning and ends on Tuesday at lunch

The [Dune Developer Meeting 2015](../2015-09-devmeeting)
takes place afterwards on September 29-30, 2015. Users are welcome to
join the discussions.

### Get together

On Sunday evening 7:30 pm, we are having a get together. It takes place
at the Restaurant “Das Bootshaus”, Schurmanstr. 2, 69115 Heidelberg.
Please indicate in the participation table whether you would be
interested to join.

Schedule
--------

Every presentation will get a 20 minute time slot, 15 minutes talk + 5
minutes discussion.

### Sunday, September 27, 2015

|  Time    |  Title      |  Description
| -------- | ----------- | -------------
| 19:30-?? | GetTogether | [Restaurant das Bootshaus](http://www.dasbootshaus.com/) together with the [C++ User Group Rhein Neckar](http://www.meetup.com/C-User-Group-Rhein-Neckar/events/223991168/)

### Monday, September 28, 2015

  Time        |  Title / Person                    | Description / Talk Title
 ------------ | ---------------------------------- | -------------------------
  9:00-9:20   | Welcome                            | Short introduction of participants (Markus Blatt)
  9:20-10:40  | User Presentations                 | Chair: Bernd Flemisch
              | S. Girke                           | [*Atherosclerosis, Multiphysics and DUNE-FEM-DG*](http://wwwmath.uni-muenster.de/u/stefan.girke/talks/duneUser2015.pdf)
              | L. Lubkoll                         | [*FunG - Biomechanical Models in Modern C++*](/pdf/meetings/2015-09-usermeeting/Lars_Lubkoll_FunG.pdf)
              | J. Vorwerk                         | *New approaches to solve the EEG forward problem with DUNE-PDELab*
              | A. Nüßing                          | [*Unfitted Discontinuous Galerkin in Brain Research and the Topology Preserving Marching Cubes*](/pdf/meetings/2015-09-usermeeting/dune_user_meeting_2015_nuessing.pdf)
  10:40-11:10 | Coffee Break                       |
  11:10-12:30 | User Presentations                 | Chair: N.N. / core developer
              | D. Kempf                           | [*dune-testtools: Modelling dynamic and static variations of system tests through ini files*](/pdf/meetings/2015-09-usermeeting/Dominic\_Kempf\_testtools.pdf)
              | R. Milk                            | *Developing DUNE-Multiscale: Tools, Workflow and Results*
              | R. Kloefkorn                       | *OPM and DUNE in Scientific and Industrial Applications*
              | S. Müthing                         | *What’s this EXA-DUNE anyway and what the heck are people doing in there?*
  12:30-14:00 | Lunch break                        | Volkswirtschaft of [Tati](http://www.restaurant-tati.de/inhalt.php?id=1913&menu_level=1&id_mnu=1913&id_kunden=300)
  14:00-14:30 | User Polls / Dune Users statistics | Bernd Flemisch
  14:30-15:30 | *dune-functions* presentations     | Chair: Christoph Grüninger
              | O. Sander                          | [*Function space bases in the dune-functions module*](/pdf/meetings/2015-09-usermeeting/sander-function-space-bases.pdf)
              | C. Gräser                          | [*Type-erased interfaces in dune-functions*](/pdf/meetings/2015-09-usermeeting/dune-user-meeting-2015-graeser.pdf)
              | C. Engwer                          | [*pdelab cleanup in the light of dune-functions*](/pdf/meetings/2015-09-usermeeting/engwer_presentation.pdf)
  15:30-16:00 | Coffee Break                       |
  16:00-17:40 | User Presentations                 | Chair: Robert Klöfkorn
              | F. Gruber                          | [*dune-dpg: A Library for Petrov–Galerkin Using Optimal Test Spaces*](/pdf/meetings/2015-09-usermeeting/dune-dpg.pdf)
              | A. Klewinghaus                     | [*dune-dpg: A Library for Petrov–Galerkin Using Optimal Test Spaces*](/pdf/meetings/2015-09-usermeeting/dune-dpg.pdf)
              | C.-J. Heine                        | [*ACFem — Adaptive Convenient Finite Elements with Dune::Fem*](/pdf/meetings/2015-09-usermeeting/dum15-cjh.pdf)
              | E. Mueller                         | [*Matrix-free block-Jacobi preconditioners for higher-order DG methods*](/pdf/meetings/2015-09-usermeeting/slidesEikeMueller.pdf)
              | M. Nolte                           | *Finite Volumes on Arbitrary Polynomial Grids in DUNE*
  19:30       | Dinner at Klosterhof Neuburg       | http://www.klosterhof-neuburg.de

### Tuesday, September 29, 2015

| Time       | Title / Person | Description / Talk Title
| ---------- | -------------- | ------------------------
| 9:00-10:20 | User Presentations | Chair: Steffen Müthing
| | M. Zabiégo | [*Use of the DUNE environment for the development of a vapor explosion calculation tool*](/pdf/meetings/2015-09-usermeeting/DUNE_sept_2015_CEA.pdf)
| | T. Malkmus | *Coupling DUNE grids with different dimensions: A FSI application*
| | L. Riedel | *DORiE - Solving the Richards Equation with DUNE-PDELab*
| | B. Kane | [*Discontinuous Galerkin Methods for Porous-Media Flow with DUNE-FEM*](/pdf/meetings/2015-09-usermeeting/video_version_user_meetingold.pdf)
| 10:20-10:50 | Coffee Break |
| 10:50-11:50 | User Presentations | Chair: Christian Engwer
| | T. Koch | [*dune-foamgrid*](/pdf/meetings/2015-09-usermeeting/foamgriddune2015.pdf)
| | A. Fomins | [*Introduce the curvilinear grid*](/pdf/meetings/2015-09-usermeeting/fomins-presentation-dune-user-meeting.pdf)
| | M. Alkämper | [*Distributed Newest Vertex Bisection (in dune-ALUGrid)*](/pdf/meetings/2015-09-usermeeting/distributedNVB.pdf)
| 11:50-12:20 | DUNE Discussion | What users anticipate from further development (Markus Blatt)
| 12:20-12:30 | Farewell |
| 12:30-14:00 | Lunch Break | Volkswirtschaft of [Tati](http://www.restaurant-tati.de/inhalt.php?id=1913&menu_level=1&id_mnu=1913&id_kunden=300)
| 14:00 | [Dune Developer Meeting](../2015-09-devmeeting) |

Location
--------

### Meeting Venue

The meeting will take place close to Heidelberg main station at

[Dezernat 16](http://www.dezernat16.de)\
Emil-Maier-Straße 16\
69115 Heidelberg\
Germany

Room: Former Turnhalle on the fourth floor (German: 3. Stock).

The entrance of the building is on the right side of it, i.e. right to
[Cafe Leitstelle](http://www.dezernat16.de/veranstaltungen/leitstelle/).

It is just a few minutes to walk here from the main station. If you exit
the main station keep left until the second traffic light (you will see
a bridge crossing the rails in front of you, the station is behind you).
Turn right here. Dezernat 16 (the former fire departement) is on the
left after the next traffic light, and directly before the gas station.

Markus blocked some rooms for Sept. 27-30 at the [Ibis
hotel](http://www.ibis.com/gb/hotel-1447-ibis-heidelberg-hauptbahnhof/index.shtml)
close to the main station. The rooms are blocked until July 20. Single
room incl. breakfast costs 88 €, double room 109 € (both fully flexible
rates). Please reserve your room directly with the hotel either by
telephone, email, or fax. Make sure to mention the keyword “DUNE
Meeting”. Note that this is not a special price but just to make sure
that there are at least 20 rooms available. The street price might be
competitive.

### Venue GetTogether 7:30 p.m. Sunday Sep. 27, 2015

The GetToGether will take place at the Restaurant “Das Bootshaus”,
Schurmanstr. 2, 69115 Heidelberg. It it to the right of the Thermalbad
and about a 10 minute walk from the main station.

### Venue Dinner 7:30 p.m. Monday Sep. 28, 2015

The Dinner on Monday will happen at Klosterhof Neuburg, which is a
little bit outside of Heidelberg near Ziegelhausen. One can easily reach
it from the main station using bus number 34 (direction
Heiligkreuzsteinach). Exit at stop “Ziegelhausen, Stift Neuburg” and
walk up the hill. After two curves of the street you are there. Buses
leave 9 and 49 minutes past the hour and take about 15 minutes. For a
complete bus schedule see the [VRN website](http://fahrplanauskunft.vrn.de/vrn/XSLT_TRIP_REQUEST2?language=en&sessionID=0&execInst=normal&useRealtime=1&calcOneDirection=1&type_origin=any&type_destination=any&delMinDistTrips=1&itOptionsActive=1&ptOptionsActive=1&lineRestriction=402&locationServerActive=1&defaultOdvType=any&coordListOutputFormat=STRING&verifyAnyLocViaLocServer=1&tryToFindLocalityStops=1&convertStopsPTKernel2LocationServer=1&anySigWhenPerfectNoOtherMatches=1&anyMaxSizeHitList=550&anyHitListReductionLimit=40&nameValue_origin=&placeValue_origin=&nameInfo_origin=invalid&placeInfo_origin=invalid&nameState_origin=empty&placeState_origin=empty&execStopList_origin=0&nameValue_destination=&placeValue_destination=&nameInfo_destination=invalid&placeInfo_destination=invalid&nameState_destination=empty&placeState_destination=empty&execStopList_destination=0&name_origin=hauptbahnhof+heidelberg&name_destination=stift+neuburg&datum=Mo%2C+28.9.2015&itdDateDay=28&itdDateMonth=9&itdDateYear=15&itdTime=19%3A30&itdTripDateTimeDepArr=arr)\
The buses back to Heidelberg leave every hour at 21 and 51 minutes after
the hour until 11:51 p.m.

Map
---

![](http://staticmap.openstreetmap.de/staticmap.php?center=49.4065,8.680,lightblue3,-73.998672&zoom=16&size=865x512&maptype=mapnik&markers=49.40940,8.68114|49.40704,8.6723|49.40758,8.67504)

A map created with http://staticmap.openstreetmap.de. Marker 1 is “Das
Bootshaus” where the get-together on Sunday takes place, marker 2 is the
Dezernat 16, and marker 3 is Volksküche at Restaurant Tati where the
lunch breaks will take place. To get to Tati walk left from Dezernat 16,
at the traffic light turn right. Cross the next street and turn right at
the tram station and keep right afterwards.

Participants
------------

Please add your name below (ordered by last name) or send the necessary
information (name, institution, and presentation title) via email to
<gruenich@dune-project.org>.

All further communication is done via the Dune user mailing list
<dune@dune-project.org>; please follow the list

Name | Institution | Country | Get together | Tentative Presentation Title
---- | ----------- | ------- | ------------ | ----------------------------
  Martin Alkämper      | University of Stuttgart, IANS            | Germany     | yes          | *Distributed Newest Vertex Bisection (in dune-ALUGrid)*
  Peter Bastian        | University of Heidelberg, IWR            | Germany     | ?            | —
  Markus Blatt         | Blatt HPC-Simulation-Software & Services | Germany     | yes          | —
  Ansgar Burchardt     | TU Dresden                               | Germany     | probably     | —
  Andreas Dedner       | Warwick                                  | UK          | probably     | —
  Christian Engwer     | Uni Münster                              | Germany     | yes          | *pdelab cleanup in the light of dune-functions*
  Jorrit Fahlke        | Uni Münster                              | Germany     | yes          | —
  Andreas Fischle      | TU Dresden                               | Germany     | yes          | —
  Bernd Flemisch       | University of Stuttgart, LH2             | Germany     | no           | —
  Christophe Fochesato | CEA Cadarache                            | France      | no           | —
  Aleksejs Fomins      | LSPR AG                                  | Switzerland | probably     | *Introduce the curvilinear grid*
  Stefan Girke         | Uni Münster                              | Germany     | yes          | *Atherosclerosis, Multiphysics and DUNE-FEM-DG*
  Carsten Gräser       | FU Berlin                                | Germany     | ?            | *Type-erased interfaces in dune-functions*
  Felix Gruber         | RWTH Aachen                              | Germany     | yes          | *dune-dpg: A Library for Petrov–Galerkin Using Optimal Test Spaces*
  Christoph Grüninger  | University of Stuttgart, LH2             | Germany     | no           | —
  Claus-Justus Heine   | University of Stuttgart, IANS            | Germany     | probably     | *ACFem — Adaptive Convenient Finite Elements with Dune::Fem*
  Birane Kane          | University of Stuttgart, IANS            | Germany     | no           | *Discontinuous Galerkin Methods for Porous-Media Flow with DUNE-FEM*
  Dominic Kempf        | University of Heidelberg, IWR            | Germany     | yes          | *dune-testtools: Modelling dynamic and static variations of system tests through ini files*
  Angela Klewinghaus   | RWTH Aachen                              | Germany     | yes          | *dune-dpg: A Library for Petrov–Galerkin Using Optimal Test Spaces*
  Robert Kloefkorn     | IRIS                                     | Norway      | no           | *OPM and DUNE in Scientific and Industrial Applications*
  Timo Koch            | University of Stuttgart, LH2             | Germany     | no           | *dune-foamgrid*
  Christoph Lehrenfeld | Uni Muenster                             | Germany     | yes          | —
  Lars Lubkoll         |                                          | Germany     | yes          | *FunG - Biomechanical Models in Modern C++*
  Tobias Malkmus       | University of Freiburg                   | Germany     | yes          | *Coupling DUNE grids with different dimensions: A FSI application*
  René Milk            | Uni Münster                              | Germany     | no           | *Developing DUNE-Multiscale: Tools, Workflow and Results*
  Eike Mueller         | University of Bath                       | UK          | yes          | *Matrix-free block-Jacobi preconditioners for higher-order DG methods*
  Steffen Müthing      | Heidelberg University                    | Germany     | yes          | *What’s this EXA-DUNE anyway and what the heck are people doing in there?*
  Martin Nolte         | University of Freiburg                   | Germany     | yes          | Finite Volumes on Arbitrary Polygonal Grids in DUNE
  Andreas Nüßing       | Uni Münster                              | Germany     | yes          | *Unfitted Discontinuous Galerkin in Brain Research and the Topology Preserving Marching Cubes*
  Marian Piatkowski    | University of Heidelberg                 | Germany     | yes          | —
  Lukas Riedel         | University of Heidelberg, IUP            | Germany     | yes          | *DORiE - Solving the Richards Equation with DUNE-PDELab*
  Oliver Sander        | TU Dresden                               | Germany     | yes          | *Function space bases in the dune-functions module*
  Johannes Vorwerk     | Uni Münster                              | Germany     | no           | *New approaches to solve the EEG forward problem with DUNE-PDELab*
  Magali Zabiégo       | CEA Cadarache                            | France      | no           | *Use of the DUNE environment for the development of a vapor explosion calculation tool*

Proceedings
-----------

A special issue in the
[Archive of Numerical Software](http://www.archnumsoft.org/) will be
dedicated to the User Meeting Proceedings. It will be edited by
Markus Blatt, Bernd Flemisch and Oliver Sander.\
Manuscripts should be submitted until **31.1.16**. Please follow the
[ANS Author Guidelines](http://journals.ub.uni-heidelberg.de/index.php/ans/about/submissions#authorGuidelines).

  Authors                                      | Tentative Title
  -------------------------------------------- | ---------------
  M. Alkämper                                  | *Using Dune::ACFem* — *A non-smooth minimization problem*
  A. Dedner, S. Girke, R. Klöfkorn, T. Malkmus | *Dune-Fem-Dg — A modular toolbox for Solving PDEs*
  A. Dedner, A. Radcliffe                      | *A module for parallel coupling of surface bulk PDEs for both FEM FEM and FEM BEM formulations*
  F. Gruber, A. Klewinghaus, O. Mula           | *The dune-dpg library for solving PDEs with Discontinuous Petrov&ndash;Galerkin finite elements*
  C. Grüninger, T. Fetzer et al.               | *Coupling of Stokes and Darcy flow*
  C.-J. Heine                                  | *ACFem* — *Adaptive Convenient Finite Elements with Dune::Fem*
  B. Kane                                      | *Adaptive higher order Discontinuous Galerkin methods for strongly heterogeneous porous-media flow with DUNE-FEM*
  D. Kempf, T. Koch, P. Bastian, B. Flemisch   | *Automated system tests for quality assurance of numerical software at the example of Dune*
  R. Klöfkorn, M. Nolte                        | *Finite Volumes on Polyhedral Grids in DUNE*
  L. Lubkoll                                   | *FunG — Invariant-based modeling*
  T. Leibner, R. Milk, F. Schindler            | *Extending DUNE: The dune-xt modules*
  O. Sander, T. Koch, N. Schröder, B. Flemisch | *The Dune FoamGrid implementation for surface and network grids*
  C. Engwer, C. Gräser, S. Müthing, O. Sander  | *The interface for functions in the dune-functions module*

Organizers
----------

The Dune user meeting 2015 is organized by Markus Blatt, Bernd Flemisch
and Christoph Grüninger.
