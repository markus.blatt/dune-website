+++
title = "Issue Tracker"
[menu.main]
parent = "dev"
weight = 4
+++

## Bug Tracking System

DUNE retired its FlySpray bug tracker, we [migrated the old issues to GitLab](https://gitlab.dune-project.org/flyspray/FS/issues).

### GitLab Issue Tracker

All development has been moved to a [GitLab instance](https://gitlab.dune-project.org),
which also contains individual issue trackers for each DUNE core module:

* [dune-common](https://gitlab.dune-project.org/core/dune-common/issues/)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/issues/)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/issues/)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/issues/)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/issues/)
