+++
title = "Recent Changes"
[menu.main]
parent = "dev"
weight = 4
+++

## Recent changes in Git master (will become DUNE 2.6)

### Dependencies

(The dependencies have not changes so far but are listed here for completeness.)

In order to build this version of DUNE you need at least the following software:

* CMake 2.8.12 or newer
* pkg-config
* A standard complient C++ compiler supporting C++11 and the C++14 feature set of GCC 4.9.
  We support GCC 4.9 or newer and Clang 3.8 or newer. We try to stay compatible to ICC 15.1 and newer
  but this is not tested.

## Known Bugs

A list of all bugs can be found in our [issue tracker](/dev/issues).
