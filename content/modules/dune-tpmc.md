+++
group = "extension"
module = "dune-tpmc"
requires = ["dune-common"]
short = "Provides a topology preserving implementation of the marching cubes and marching simplex algorithm. This module can be used to implement cut-cell algorithms on top of the Dune interface."
maintainers = "[Christian Engwer](mailto:christi@dune-project.org), [Andreas Nüßing](mailto:andreas.nuessing@uni-muenster.de)"
git = "https://gitlab.dune-project.org/extensions/dune-tpmc"
title = "dune-tpmc"
+++

<figure class="figure center-block img-thumbnail" style="margin:30px">
<img class="figure-img img-fluid img-rounded" src="/img/test_levelset.png" style="width:200px;height:auto"></td>
<img class="figure-img img-fluid img-rounded" src="/img/test_lewiner.png" style="width:200px;height:auto"></td>
<img class="figure-img img-fluid img-rounded" src="/img/test_our.png" style="width:200px;height:auto"></td>
<figcaption class="figure-caption text-xs-right">Exact level-set, reconstruction using the standard MC33 algorithm and reconstruction with the Topology Preserving Marching Cubes.</figcaption>
</figure>

Given a scalar P1/Q1 function Φ on a domain Ω, we define a partition of Ω into two sub-domains {Ω1, Ω2} with a common interface Γ. The interface is given as the zero level-set of the scalar function Φ. The marching cubes algorithm computes a piece-wise linear reconstruction of Γ.

The _dune-mc_ modules not only computes a reconstruction of the interface, but also further information. The user can access the following information:

* Reconstruction of the interface Γ
* Reconstruction of Ω1
* Reconstruction of Ω2
* Connectivity pattern w.r.t. each sub-domain

In order to allow the algorithm to be employed in Finite Element simulations, a reconstruction has to fullfill certain topological guarantees:

* The connectivity pattern of the cell vertices must be preserved within each subentity. In particular this means that vertices connected along an edge, face or volume, should still be connected via the same subentity.
* The exact interface Γ partitions each grid cell into patches belonging to either Ω1 or Ω2. We require that number of patches and their domain association is the same in the polygonal reconstruction.
* The vertices of the reconstructed interface lie on the exact zero level-set.

### Publications and Documentation
The first concepts of _dune-mc_ are published in

* {{% doi doi="10.1002/nme.2631" %}}
* Ch. Engwer, An Unfitted Discontinuous Galerkin Scheme for Micro-scale Simulations and Numerical Upscaling, Heidelberg University, 2009. ([PDF](http://www.ub.uni-heidelberg.de/archiv/9990))

We are working on a more thorough publication which will also cover
the improved _Topology Preserving Marching Cubes Algorithm_. A
preprint is available from Arxiv:

* C. Engwer, A. Nüßing , Geometric Integration Over Irregular Domains with topologic Guarantees, 2016 ([arXiv:1601.03597](https://arxiv.org/abs/1601.03597))

### Maintainers

dune-mc has been written by [Christian Engwer](http://wwwmath.uni-muenster.de/u/christian.engwer/) and [Andreas Nüßing](http://wwwmath.uni-muenster.de/num/Arbeitsgruppen/ag_engwer/organization/nuessing/).
