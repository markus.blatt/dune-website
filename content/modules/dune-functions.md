+++
git = "https://gitlab.dune-project.org/staging/dune-functions"
group = "extension"
module = "dune-functions"
requires = ["dune-localfunctions", "dune-grid", "dune-typetree"]
short = "Abstractions for functions and discrete function space bases"
suggests = ["dune-istl"]
title = "dune-functions"

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/dune-functions/releases2.5", "/doxygen/dune-functions/master"]
doxygen_modules = ["dune-functions"]
doxygen_branch = ["releases/2.5", "master"]
doxygen_name = ["DUNE-FUNCTIONS", "DUNE-FUNCTIONS"]
doxygen_version = ["2.5", "unstable"]
+++

### dune-functions
The _dune-functions_ module provides an abstraction layer for global finite element functions. Its two main concepts are functions implemented as callable objects, and bases of finite element spaces.

#### Functions
_dune-functions_ provides an interface to "functions" in the mathematical sense, in particular to finite element functions defined on a grid, but going far beyond that.

The interface revolves around the concept of a "callable". It encompasses any type of C++ object that can be evaluated with `operator()`, like free functions, function objects, and even C++11 lambdas. Dynamic polymorphism is realized using type erasure and the `std::function` class, which does not sacrifice efficiency in purely static code.

_dune-functions_ extends the "callable" concept into several directions. First, it allows for differentiable functions. Such functions can hand out their derivative as new function objects. Second, for functions defined piecewisely on a finite element grid, the concept of local function is introduced. Local functions can be bound to grid elements. All further evaluations of a function bound to an element are in local coordinates of that element. This approach allows to avoid overhead when there are many consecutive evaluations of a function on a single element.

#### Function Space Bases
The second part of _dune-functions_ provides a well-defined interface to bases of finite element function spaces. For this interface, a finite element basis is a set of functions with a prescribed ordering, and a way to index them. The core functionality has three parts:

1. For a given grid element, obtain the restrictions of all basis functions to this element, except for those functions where the restriction is zero. In other words: get the shape functions for the element.
2. Get a local numbering for these shape functions. This is needed to index the element stiffness matrix.
3. Get a global numbering for the shape functions. This is needed to index the global stiffness matrix.

While local numbers are always integers, global numbers can be multi-indices, if appropriate.

A central feature is that finite element bases for vector-valued and mixed spaced can be constructed by tensor multiplication of simpler bases. The resulting expressions can be interpreted as tree structures. For example, the tree for the three-dimensional Taylor-Hood basis is shown below. This tree structure is directly exposed in the _dune-functions_ interface.

<img src="/img/dune-functions-taylor-hood-tree.svg" alt="Taylor-Hood space in tree representation" width="400" height="300">


#### Implementations of Function Space Bases
Currently, the following finite element bases are available:

* PQkNodalBasis: A k-th order Lagrangian bases, with k a compile-time parameter.
* LagrangeDGBasis: A k-th order DG basis, using Lagrangian shape functions.
* TaylorHoodBasis: The P2/P1 Taylor-Hood basis, for simplex-, cube-, and mixed grids.
* BSplineBasis: A basis of B-Spline functions of given arbitrary order on a structured grid.

### Download
You can download the current development version using anonymous git.

`git clone https://gitlab.dune-project.org/staging/dune-functions.git`

The latest release is _dune-functions_ 2.5, available from the Dune [download page](/releases/2.5.0).
It is to be used with the 2.5 release of the Dune core modules.
If you would like to use _dune-functions_ together with the Dune 2.4 release, you can use the
_releases/2.4-compatible_ branch from the git repository mentioned above.


_dune-functions_ depends on the _dune-typetree_ module, also available from the [download page](/releases/2.5.0).
Please install that before trying to install _dune-functions_.


### Documentation
The class documenation generated with doxygen is available online:
* [class documentation for 2.5 release branch](https://www.dune-project.org/doxygen/dune-functions/releases2.5)
* [class documentation for development branch](https://www.dune-project.org/doxygen/dune-functions/master)

The module contains a manual that can be build using `make doc`. Furtermore it contains example programs in the `examples/` directory.



### Mailing lists
_dune-functions_ development and discussions happen mainly on the [dune-functions mailing list](http://lists.dune-project.org/mailman/listinfo/dune-functions).

### Maintainers
_dune-functions_ has been mainly written by

1. Christian Engwer
2. Carsten Gräser
3. Steffen Müthing
4. Oliver Sander

See the COPYING file contained in the source code for a complete list.

We welcome interest and contributions by additional developers.
