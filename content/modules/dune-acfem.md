+++
module = "dune-acfem"
group = "user"
requires = ["dune-common", "dune-geometry","dune-grid","dune-fem"]
suggests = ["dune-alugrid", "dune-istl", "dune-localfunctions", "dune-spgrid"]
title = "dune-acfem"
git = "https://gitlab.dune-project.org/dune-fem/dune-acfem"
short = "An add-on to DUNE-FEM which aims at a convenient discretization of diffusion dominated problems with Adaptive Conforming Finite Elements."

doxygen_url = ["/doxygen/dune-acfem/release-2.4"]
doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-fem","dune-acfem"]
doxygen_branch = ["releases/2.4"]
doxygen_name = ["DUNE-ACFEM"]
doxygen_version = ["2.4.1"]
+++

An add-on to [DUNE-FEM](/modules/dune-fem) which aims at a convenient
discretization of diffusion dominated problems with Adaptive
Conforming Finite Elements. It is in spirit and origin based on the
[DUNE-FEM-HOWTO](/modules/dune-fem-howto). "Conforming" means "continuous" if the
non-discrete problem lives in an H1-context.

The idea was to factor out common code for the cG-FEM examples into a
(template-) library. ACFem adds the possibility to build complicated
models by forming algebraic expressions from a zoo of basic "atom"
models like

```cxx
myModel = laplaceModel + sinXFct*massModel + dirichletModel - rhsFct;
```

Care has been taken to do this in a way that the resulting code
exhibits a decent performance by eliminating redundant "zero
expressions" with template specializations. Still it is always
possible to simply code the entire PDE in question into one monolithic
model, or to augment the "model-zoo" by own models and combine those
"hand-coded" models with some of the existing ones. An autematically generated Doxygen documentation can be found [here](/doxygen/dune-acfem/release-2.4).

With respect to the code base present in the Poisson- and
Heat-equation examples from the DUNE-FEM-HOWTO ACFem adds more
general boundary conditions and adaptive algorithms which resemble the
state of the Fem-Toolbox [Alberta](http://www.alberta-fem.de) in this
respect.

ACFem requires a compiler with decent C++-11 support. Expect gcc-4.7
to fail. gcc >= 4.8.1 should work, as well as recent clang versions.
