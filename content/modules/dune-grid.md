+++
date = "2016-02-10T15:28:57+01:00"
git = "https://gitlab.dune-project.org/core/dune-grid"
group = "core"
module = "dune-grid"
requires = ["dune-common"]
short = "The Dune grid interface and some grid implementations"
maintainers = "The Dune Core developers <dune@dune-project.org>"
title = "dune grid"

+++
