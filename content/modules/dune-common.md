+++
git = "https://gitlab.dune-project.org/core/dune-common"
group = "core"
maintainers = "The Dune Core developers <dune@dune-project.org>"
module = "dune-common"
short = "Basic infrastructure classes for all Dune modules"
title = "dune-common"
+++
