+++
git = "https://gitlab.dune-project.org/core/dune-localfunctions"
group = "core"
maintainers = "The Dune Core developers <dune@dune-project.org>"
module = "dune-localfunctions"
requires = ["dune-common"]
short = "Provides interface and implementation for shape functions defined on the DUNE reference elements. In addition to the shape function, interpolation operators and special keys are provided which can be used to assemble global function spaces on finite-element grids."
title = "dune localfunctions"
+++
