+++
module = "dune-polygongrid"
group = "grid"
requires = ["dune-grid"]
maintainers = "Martin Nolte"
git = "https://gitlab.dune-project.org/martin.nolte/dune-polygongrid"
short = "PolygonGrid implements a DUNE grid consisting of polygons."
+++

Dune-PolygonGrid
================

The DUNE module dune-polygongrid provides an implementation of the DUNE grid
interface for grids consisting of polygons.
