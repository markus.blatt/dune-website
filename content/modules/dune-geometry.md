+++
git = "https://gitlab.dune-project.org/core/dune-geometry"
group = "core"
module = "dune-geometry"
requires = ["dune-common"]
maintainers = "The Dune Core developers <dune@dune-project.org>"
short = "Includes everything related to the DUNE reference elements. This includes the reference elements themselves, mappings on the reference elements (geometries), and quadratures."
title = "dune geometry"
+++
