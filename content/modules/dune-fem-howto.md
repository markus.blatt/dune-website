+++
module = "dune-fem-howto"
group = "user"
requires = ["dune-common", "dune-geometry","dune-grid","dune-fem"]
suggests = ["dune-alugrid", "dune-istl", "dune-localfunctions", "dune-spgrid"]
title = "dune-fem-howto"
git = "https://gitlab.dune-project.org/dune-fem/dune-fem-howto"
short = "A step by step introduction to the DUNE-FEM discretization module."

doxygen_url = ["/doxygen/dune-fem-howto/release-2.4"]
doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-fem","dune-fem-howto"]
doxygen_branch = ["releases/2.4"]
doxygen_name = ["DUNE-FEM-HOWTO"]
doxygen_version = ["2.4.1"]
+++

### General

DUNE-FEM-HOWTO contains a step by step introduction to the [DUNE-FEM module](/modules/dune-fem). It contains code on how to solve different types of partial differential equations on parallel computers with DUNE-FEM.

The first part of the tutorial shows in 5 steps how to solve a nonlinear elliptic problem - introducing the assembly of a linear elliptic operator and then how to use this together with a Newton type scheme to solve a nonlinear problem.

The code for the linear elliptic is extended to include local adaptivity based on a residual type estimate. Furthermore a DG type discretization is discussed.

In the next step the elliptic operator is used to solve a system of nonlinear reaction-diffusion equations.

The final tutorial covers the implementation of a finite-volume scheme for a system of nonlinear conservation laws.

Further section discuss the available linear solvers, I/O and thread parallelization.

The code can be a good starting point for your own project.

### Documentation

The [documentation of DUNE-FEM-HOWTO 2.4](/doxygen/dune-fem-howto/release-2.4) is genereated using Doxygen and it is also available as a PDF [here](/download/dune-fem-howto/dune-fem-howto-2.4.0.pdf).

### Download

The full repository is hosted on our [GitLab instance](https://gitlab.dune-project.org/dune-fem/dune-fem-howto). In order to use a stable release, please check out the corresponding branch.

The last DUNE-FEM-HOWTO stable releases are also available as tar balls:
+ [DUNE-FEM-HOWTO-1.3.0.tar.gz](/download/dune-fem-howto/dune-fem-howto-1.3.0.tar.gz) - compatible with DUNE-FEM-1.3.1
+ [DUNE-FEM-HOWTO-1.4.0.tar.gz](/download/dune-fem-howto/dune-fem-howto-1.4.tar.gz) - compatible with DUNE-FEM-1.4.0
+ [DUNE-FEM-HOWTO-2.4.0.tar.gz](/download/dune-fem-howto/dune-fem-howto-2.4.tar.gz) - compatible with DUNE-FEM-2.4.0

### Known issues

All the known issues of DUNE-FEM-HOWTO can be found on the [online bug traker](https://gitlab.dune-project.org/dune-fem/dune-fem-howto/issues). Please use it to report any bug.

### Support

DUNE-FEM-HOWTO provides its own [user mailing lists](http://lists.dune-project.org/mailman/listinfo/dune-fem) and a [developer mailing lists](https://listserv.uni-stuttgart.de/mailman/listinfo/dune-fem-devel).
