+++
title = "Compilers"
[menu.main]
parent = "docs"
weight = 3
+++

Compilers
=========

The intention is to support the oldest compiler on any relevant
supercomputer. The rule of thumb is that a compiler is supported for
five years after it has been released. We may support a compiler for
more are less than five years on a case-by-case basis, if it becomes
clear that it is still needed or becomes too hard to support.

Supporting a compiler means that we will make resonably sure releases of
Dune works with it, and that errors that occur with that compiler are
considered "bugs". Fixes for errors with unsupported compilers are still
welcome, though they are considered "wishlist" or "feature request" and
they should not be too intrusive.

Of course, Dune should work with any standard complient compiler, but
there are two important points about that statement:

1.  No compiler is complete standard-complient, i.e. they do have bugs.
    Dune needs to work despite these bugs with the compilers we
    care about.
2.  Which standard are we actually talking about? The most recent standards
    are from 2011 and 2014. On the development branch we require C++11 and
    and some features of C++14.

Currently supported compilers
-----------------------------

Below you can find the requirements for the development branch.
For detailed information on the requirements of a release, have
a look at the corresponding [release notes](/releases).

### GCC

Our main development compiler is g++ the Gnu Compiler Collection. On the
development branch *master* we currently support *g++-4.9* and newer.
[Here](http://gcc.gnu.org/releases.html) is the list of release dates.

### Clang

Clang is a compiler build on top of the LLVM infrastructure and
primarily sponsored by Apple. We currently support versions 3.8
and newer.

### ICC

We try to stay compatible with icc 15.1. But this is currently
not really tested.


C++ support in common compilers
-------------------------------

Some useful links:

-   [C++11 Support in GCC](https://gcc.gnu.org/projects/cxx-status.html#cxx11)
-   [C++14 Support in GCC](https://gcc.gnu.org/projects/cxx-status.html#cxx14)
-   [C++11 Support in Clang](http://clang.llvm.org/cxx_status.html#cxx11)
-   [C++14 Support in Clang](http://clang.llvm.org/cxx_status.html#cxx14)
-   [C++0x CompilerSupport](http://wiki.apache.org/stdcxx/C%2B%2B0xCompilerSupport)

Compiler support on some supercomputers
---------------------------------------

-   [Software at the High Performance Computing Center Stuttgart (HLRS)](https://wickie.hlrs.de/platforms/index.php/Software_Development_Tools,_Compilers_%26_Libraries)
