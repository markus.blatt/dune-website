+++
[menu.main]
name = "Guides"
identifier = "guides"
parent = "docs"
weight = 0
+++

* [Copyable Entities and Intersections](copyable_entities_and_intersections)
* [Guidelines for bug reporting](bug_reporting)
* [Repository layout](repository_layout)
* [Whitespace hook](whitespace_hook)
* [Git best practices](git_best_practices)
