+++
title = "Installing SuperLU"
+++
### Installing SuperLU for DUNE
An updated short HowTo for the installation of SuperLU for and with Dune, thanks to Uli Sack. (This description is known to have been working for SuperLU 4.2 and 4.3 with the dune trunk pre2.3 on Debian squeeze.)

#### Dependencies
There are no mandatory packages for SuperLU. SuperLU requires libblas, but for systems without, the source of libblas is shipped with SuperLU.

#### Compilation and Installation
**Download**
SuperLU is available from http://crd.lbl.gov/~xiaoye/SuperLU/. Please note that there are three different version available, SuperLU (sequential), SuperLU_MT (shared memory parallel) and SuperLU_DIST (distributed memory parallel). This howto covers the installation of SuperLU.

Unpack the downloaded archive and read the README file.

**make parameters for SuperLU**
Copy the correct file `$(SuperLUroot)/MAKE_INC/make.<system>` to `$(SuperLUroot)/make.inc` . In the file `make.inc` a few changes might have to be done:

* Adapt path to SuperLU in definition of SuperLUroot
* Make sure the version number in the definitions of SuperLUroot, SUPERLULIB is correct
* Follow the instructions in the README

Inconveniently SuperLU has dependencies on Fortran which results in some potential pitfalls compiling it. The Fortran compiler appends `'_'` or `'__'` or nothing to function names (or puts everything in upper case) in the C/Fortran-interface. The correct setting has to be found and given above (e.g. 'CDEFS = -DAdd_').

'make' compiles the library

**Dune configure options**
```
--with-superlu=<superlu-path> --with-superlu-lib=<libraryname>
```
**Changes to the Makefile.am of your Dune project**
```
Add '$(SUPERLU_LIBS)' to the LDADD.
Add '$(SUPERLU_LDFLAGS)' to the LDFLAGS.
Add '$(SUPERLU_CPPFLAGS)' to the CPPFLAGS.
```

**Test**
Test the functionality by compiling and running the superlutest in `dune-istl/dune/istl/test`. Example code for how to use SuperLU in Dune may be seen there as well.
