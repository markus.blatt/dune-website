+++
date = "2016-12-23T14:14:00+01:00"
title = "Invitation to the Dune User Meeting 2017"
+++

As a second gift for christmas, we cordially invite all of our users
to the 2017 Dune User Meeting to be held at Heidelberg University on
March 13-14, 2017. The meeting is intended as an opportunity for the Dune users
to present their work, meet the developers, get into fruitful discussion and
foster future collaborations.

For more detailed information and registration, check the [event website](https://conan2.iwr.uni-heidelberg.de/events/user-meeting_2017/).

Also, the Dune developer meeting will be held at the same venue on March 15th.
