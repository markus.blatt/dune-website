+++
date = "2007-02-14"
title = "Moving the DUNE Server"
+++

We are moving DUNE to a new server. Many services (like this web page) already run on the server without anybody noticing it. Now we started to move the repositories.

Access to `dune-common`, `dune-grid`, `dune-grid-howto` and `dune-istl` on the old server is disabled and these repositories are moved to the new server.

The DNS Entry for svn.dune-project.org is updated and you should be able to access the repositories as soon as your DNS caches are uptodate. Both anonymous checkout and read/write access are available via the same url:
 `https://svn.dune-project.org/svn/dune-XXXX/`
