+++
date = "2014-06-26"
title = "PDELab 2.0.0 released"
+++

The PDELab developers are proud to announce the release of PDELab 2.0.0. PDELab 2.0 is a major release containing a large number of new features and improvements. Most importantly, the DOF handling and the interface to linear algebra have been completely rewritten, bringing improvements like faster and more memory-efficient matrix pattern construction, easier iteration over DOF vectors and built-in support for applying arbitrary permutations to the DOF order, among others. Another important new feature is the ability to do computations on non-overlapping grids without storing the ghost DOFs, reducing both the parallelization overhead and the complexity of the underlying code, in particular when using AMG. Finally, it is now possible to do adaptive calculations with nested function spaces. For details and a more complete overview of the changes and bug fixes in this release, see the [release notes](http://cgit.dune-project.org/repositories/dune-pdelab/plain/RELEASE_NOTES?h=releases/2.0) and the [PDELab homepage](/modules/dune-pdelab) for further information on how to obtain PDELab, including source packages.
