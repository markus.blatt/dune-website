+++
date = "2007-09-17"
title = "Two Papers on DUNE Published as Preprints"
+++

Today we have completed two papers on the DUNE system. They are *P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, M. Ohlberger, O. Sander.* **A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part I: Abstract Framework** and *P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, R. Kornhuber, M. Ohlberger, O. Sander:* **A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part II: Implementation and Tests in DUNE**.

The first gives a rigorous mathematical definition of a grid as we see it. The second one explains the implementation and gives a few example applications to demonstrate the advantages of DUNE. If you're using DUNE for your own publications we kindly ask you to cite the second one.

The papers have been submitted to 'Computing'. They are also available as Matheon preprints [403](http://www.matheon.de/research/show_preprint.asp?action=details&serial=403) and [404](http://www.matheon.de/research/show_preprint.asp?action=details&serial=404), respectively.
