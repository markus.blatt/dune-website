+++
date = "2014-02-10"
title = "DUNE 2.3.0 Released"
+++

<div markdown style="float:left; padding-right: 1em">
![](/img/dune23.png)
</div>
We are pleased to announce the release of the new stable version 2.3.0 of the Dune core modules.
 Dune 2.3.0 brings not only one and a half years' worth of bug fixes but also some nice additonal features. The Clang compiler is now better supported, UGGrid gained more parallel features, dune-istl got support for UMFPACK as an additional direct linear solver backend, and all modules can now be built using the experimental CMake build system.

For further improvements, deprecations, and clean-ups, have a look at the [release notes](/releases/2.3.0/).
