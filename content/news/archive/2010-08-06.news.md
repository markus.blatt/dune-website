+++
date = "2010-08-06"
title = "Dune User Meeting Announcement"
+++

Please register for the Dune User Meeting taking place int Stuttgart on October 6th-8th, 2010, at the [meetings homepage](http://dumux.org/dune-user-meeting.html).
