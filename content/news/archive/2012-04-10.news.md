+++
date = "2012-04-10"
title = "2nd DUNE User Meeting, June 21, 2012, Muenster, Germany. (Canceled)"
+++

**Update:** The DUNE User Meeting 2012 is canceled. The facts that we have three days of PDESoft before and that it overlaps with the developers meeting did not create enough interest.
 [Read the cancellation announcement](http://lists.dune-project.org/pipermail/dune/2012-May/011312.html)

After the International Workshop [PDE Software Frameworks -- 10th anniversary of DUNE](http://pdesoft2012.uni-muenster.de), June 18-20, 2012, Münster, Germany, we invite you to participate in the *2nd DUNE User Meeting*, June 21, 2012, Münster, Germany.

The venue will be the same as for the PDESoft workshop. ~~Please add your name to the list of participants at the User Wiki page.~~ You are also most welcome to post your ideas for the meeting there.

*Update: Some Wiki pages are obsolete now.*
