+++
date = "2013-04-19"
title = "2nd DUNE User Meeting, Aachen, September 25-26, 2013"
+++

We cordially invite all DUNE users to participate in the *2nd DUNE User Meeting 2013* to be held in Aachen September 25-26, 2013. This is a meeting for our users to showcase how they are using DUNE, foster future collaborations, and present their needs to the DUNE community.

The format of the meeting will be rather informal and similar to the *Finite Element Fair*. This means that every participant will be able to give a presentation. Presentations should be prepared such that the time needed for presenting is held flexible, i.e. ranging from a few minutes of presentation time to about half an hour. At the start of the meeting we will divide the available presentation time between the speakers and choose the order of the speakers randomly.

Please indicate your interest and whether you would like to give a presentation in the ~~wiki~~. Although there is no fixed deadline, we would appreciate registering as soon as possible.

*Update: Some Wiki pages are obsolete now.*
