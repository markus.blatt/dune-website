+++
title = "Legal Statements"
[menu.main]
parent = "about"
weight = 99
+++

## Copyright and License

Unless otherwise stated, the contents of this website are copyright (c) 2002-2016 by the Dune developers. Notable exceptions include

- Research papers, where you should contact the authors for relevant information.
- The tarballs of software releases, where the tarballs include information about the copyright holders and license of the downloaded software.
- The automatically generated documentation, neither the Doxygen-generated [API documentation](/doxygen) nor the Sphinx-generated [buildystem documentation](/buildsystem/). As this information is extracted from the source code of the documenteds packages, the copyright is with the authors of that software, and the documentation is distributed under the license of that software.
- The source files that create the theme of this website, which are an imported and adapted version of the [Hugo website](https://gohugo.io), so they are licensed under the upstream [Apache License, Version 2.0](https://www.apache.org/licenses).

Note that this list does not claim to be complete. Moreover, this website contains resources from a number of [external projects](#external-dependencies), which belong to the authors of those external projects and are covered by the respective upstream licenses.

All information under the default copyright is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). Moreover, all source code that appears under this copyright is additionally licensed under the following license (the [Unlicense](https://unlicense.org)):

~~~css
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>
~~~

### Source code

The source code of this website is available at <https://gitlab.dune-project.org/infrastructure/dune-website>. Please take a look at the repository if you want to know the exact list of copyright holders for each file as well as the applicable license terms (see [LICENSE.md](https://gitlab.dune-project.org/infrastructure/dune-website/tree/master/LICENSE.md) for an overview of the licenses for files in different parts of the source tree as well as the location of imported projects by external developers).


### External dependencies

This website uses a number of external projects:

- The [Hugo](https://gohugo.io) static website generator.
- The website theme is based on the [Hugo](https://gohugo.io) website, which has been imported into the source code repository of this website. The Hugo website, which is freely available from their source code repository, is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses). As a consequence, the imported files as well as all of our theme extensions are licensed under this license.
- The [jQuery](https://jquery.org) JavaScript library, distributed under the [jQuery License](https://jquery.org/license/).
- The [Bootstrap](https://getbootstrap.com) framework, distributed under the [MIT License](https://github.com/twbs/bootstrap/blob/master/LICENSE).
- [Font Awesome](https://fontawesome.io), distributed under the [SIL Open Font License 1.1](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web) and the [MIT License](https://opensource.org/licenses/MIT).
- The [InstantClick](http://instantclick.io) JavaScript library, distributed under the [MIT License](https://instantclick.io/license.html).
- The [highlight.js](https://highlightjs.org) JavaScript source code highlighting library, distributed under the [BSD License](https://github.com/isagalaev/highlight.js/blob/master/LICENSE).
- API documentation generated using [Doxygen](http://doxygen.org).
- Build system documentation generated using [Sphinx](http://www.sphinx-dox.org).


## Contact us

In case of questions about the website or any of its contents, please contact us at <webmaster@dune-project.org>.

## Impressum

Verantwortlich für <https://dune-project.org> im Sinne des Presserechts:

Prof. Dr. Peter Bastian, Interdisziplinäres Zentrum für Wissenschaftliches Rechnen, Universität Heidelberg, Im Neuenheimer Feld 205, 69120 Heidelberg.

### Hinweis für Benutzer in Deutschland

Die Autoren der Dune-Website übernehmen keinerlei Garantie für die Richtigkeit, Vollständigkeit und Aktualität der bereitgestellten Informationen. Insbesondere übernehmen die Autoren keinerlei Haftung für Inhalte, die als fremde Inhalte gekennzeichnet und sind nicht dafür verantwortlich, dass solche Inhalte vollständig, aktuell, richtig und rechtmässig sind und nicht in unzulässiger Weise in die Rechte Dritter eingreifen. Dies gilt insbesondere auch für externe Webseiten, auf die durch einen Link verwiesen wird.
