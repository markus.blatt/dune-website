+++
title = "Publications and Talks"
[menu.main]
parent = "about"
weight = 6
+++

## Publications and Talks

### Main Publications
#### How to cite the DUNE core modules

If you used the DUNE core modules for your publication, we kindly ask you to cite the following
peer reviewed papers ([bibtex entries](/publications/dune-references-bib)). If you used DUNE 2.4,
please also cite its published release notes:

* M. Blatt, A. Burchardt, A. Dedner, C. Engwer, J. Fahlke, B. Flemisch, C. Gersbacher, C. Gräser,
  F. Gruber, C. Grüninger, D. Kempf, R. Klöfkorn, T. Malkmus, S. Müthing, M. Nolte,
  M. Piatkowski, O. Sander.
  The Distributed and Unified Numerics Environment, Version 2.4.
  Archive of Numerical Software, 4(100), 2016, pp 13 – 29, DOI: 10.11588/ans.2016.100.26526.
  [PDF](https://journals.ub.uni-heidelberg.de/index.php/ans/article/download/26526/24049)

#### dune-grid

* P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, M. Ohlberger, O. Sander. A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part I: Abstract Framework. Computing, 82(2-3), 2008, pp. 103-119. [Preprint](http://www.matheon.de/research/show_preprint.asp?action=details&serial=403)
* P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, R. Kornhuber, M. Ohlberger, O. Sander. A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part II: Implementation and Tests in DUNE. Computing, 82(2-3), 2008, pp. 121-138. [Preprint](http://www.matheon.de/research/show_preprint.asp?action=details&serial=404)

#### dune-istl

* M. Blatt, P. Bastian. The Iterative Solver Template Library. In B. Kåström, E. Elmroth, J. Dongarra and J. Wasniewski, Applied Parallel Computing. State of the Art in Scientific Computing. Volume 4699 of Lecture Notes in Scientific Computing, pages 666-675. Springer, 2007. [PDF-Preprint (149K)](/publications/istl_para06.pdf)
* P. Bastian, M. Blatt. On the Generic Parallelisation of Iterative Solvers for the Finite Element Method In Int. J. Computational Science and Engineering,4(1):56-69, 2008. [PDF - Preprint (229K)](/publications/parallel_istl_paper_ijcse.pdf)

#### dune-fem

* A. Dedner, R. Klöfkorn, M. Nolte, M. Ohlberger A generic interface for parallel and adaptive scientific computing: Abstraction principles and the DUNE-FEM module. Computing Vol. 90, No. 3, pp. 165--196, 2011 [Preprint](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/dkno_dunefempreprint.pdf).

### Other publications

* Peter Bastian, Mark Droske, Christian Engwer, Robert Klöfkorn, Thimo Neubauer, Mario Ohlberger, Martin Rumpf. Towards a Unified Framework for Scientific Computing. In Proc. of the 15th International Conference on Domain Decomposition Methods (2005), [PDF 376K](/publications/TM105-frame.pdf)
* Adrian Burri, Andreas Dedner, Robert Klöfkorn, Mario Ohlberger. An efficient implementation of an adaptive and parallel grid in DUNE. In Proc. of the 2nd Russian-German Advanced Research Workshop on Computational Science and High Performance Computing (2005), [PDF 1.8M](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/papers/paper_bdko.pdf)
* Adrian Burri, Andreas Dedner, Dennis Diehl, Robert Klöfkorn, Mario Ohlberger. A general object oriented framework for discretizing nonlinear evolution equations. In: Proceedings of The 1st Kazakh-German Advanced Research Workshop on Computational Science and High Performance Computing, Almaty, Kazakhstan, September 25 - October 1, 2005., [PDF 0.4M](http://wwwmath1.uni-muenster.de/u/ohlberger/postscript/paper_bddko.pdf)
* Peter Bastian, Markus Blatt, Christian Engwer, Andreas Dedner, Robert Klöfkorn, Sreejith P. Kuttanikkad, Mario Ohlberger, Oliver Sander. The Distributed and Unified Numerics Environment (DUNE). In Proc. of the 19th Symposium on Simulation Technique in Hannover, September 12 - 14, 2006 [PDF 3,5M](/publications/dune_asim2006.pdf)
* C. Gräser, O. Sander. The dune-subgrid Module and Some Applications. Computing, 8(4), 2009, pp. 269-290. [Preprint](http://www.matheon.de/preprints/5868_graeser_sander_subgrid.pdf)
* P. Bastian, G. Buse, O. Sander Infrastructure for the Coupling of Dune Grids. accepted in Proceedings of the 8th Enumath conference. [PDF](http://page.mi.fu-berlin.de/sander/publications/bastian_buse_sander.pdf)
* M. Blatt, P. Bastian C++ components describing parallel domain decomposition and communication. International Journal of Parallel, Emergent and Distributed Systems, 24(6), 2009, pp. 467 - 477.

## Talks

1. Robert Klöfkorn. Simulation of Fuel Cells. At Tenth International Conference on Hyperbolic Problems - Theory, Numerics, Applications (Hyp2004), Osaka (Japan) (September 2004). [PDF 2.64M](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/osaka.pdf).
2. Robert Klöfkorn. DUNE - Distributed and Unified Numerics Environment. At SIAM Scientific Computing and Engineering, Orlando (Florida) (February 2005). [PDF 3.9M](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/orlando.pdf).
* Peter Bastian DUNE - The Distributed Unified Numerics Environment. At Oberseminar Numerical Analysis and Scientific Computing (FU Berlin) (July 1th 2005) [PDF 762K](/publications/berlinJul2005.pdf).
* Robert Klöfkorn. DUNE - Distributed and Unified Numerics Environment. At Equadiff 11 - International conference on differential equations, Bratislava (Slovakia) (July 2005). [PDF 5.1M](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/bratislava.pdf).
* Robert Klöfkorn. DUNE - Distributed and Unified Numerics Environment, Die DUNE Gitterschnittstelle. At Oberseminar für Angewandte Mathematik - Freiburg (February 2006). [PDF 3.8M](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/orlando.pdf).
* Christian Engwer. DUNE - Distributed and Unified Numerics Environment. At SIAM Parallel Processing 06 (San Francisco) (February 2006). [PDF 1.4M](/publications/SIAM-PP06.pdf).
* Markus Blatt. ISTL - Iterative Solver Template Library. At International Conference on High Performance Computing (Hanoi/Vietnam) (March 2006). [PDF (0.8M)](/publications/istl_hpsc2006.pdf)
* Christian Engwer. Introduction to the Dune autobuild system. At Oberseminar SGS (Universität Stuttgart) (January 2007). [PDF 324K](/publications/dune_autobuild.pdf).
* Oliver Sander. The Distributed and Unified Numerics Environment (DUNE). At TU Dresden (January 2008). [PDF 4.3M](/publications/dune_dresden_08.pdf).
* Christian Engwer. The Distributed and Unified Numerics Environment. At Matheon-Workshop on Data Structures for Finite Element and Finite Volume Computations, FU Berlin (February 2008). [PDF 424K](/publications/matheon-workshop-08.pdf).
* Andreas Dedner The Distributed and Unified Numerics Environment (DUNE). At the opening workshop for the BEM++ project, UC London, UK (Juli 2011). [PDF 2.2M](/publications/dednerBEM.pdf).
* Andreas Dedner Construction of Higher Order Finite-Element Spaces. At the 24th Chemnitz FEM Symposium (September 2011). [PDF 1.1M](/publications/dednerChemnitz.pdf).

There are also quite a few DUNE related talks given at the [Dune User Meetings](/community/meetings).
